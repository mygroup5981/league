import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Ex2db {
    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:8080/",
                            "postgres", "12345");
            System.out.println("Opened database successfully");

            Person person1 = new Person("Bob", "bob@mail.ru");
            System.out.println(person1.hashCode());
            int id = person1.hashCode();

            String sql = null;
            stmt = c.createStatement();
            sql = "CREATE TABLE PERSONS " +
                    "(ID INT PRIMARY   KEY     NOT NULL," +
                    " USERNAME         TEXT    NOT NULL, " +
                    " EMAIL            TEXT     NOT NULL )";
            stmt.executeUpdate(sql);

            System.out.println("Table created successfully");

            sql = "INSERT INTO PERSONS (ID,USERNAME,EMAIL) " + "VALUES ("+ id +", '"+ person1.getUsername() +"', '"+ person1.getEmail() +"');";
            stmt.executeUpdate(sql);
            System.out.println("Inserted successfully");


            ResultSet rs = stmt.executeQuery( "SELECT * FROM PERSONS WHERE ID = " + id + ";" );
            while ( rs.next() ) {
                int id2 = rs.getInt("ID");
                String  username = rs.getString("USERNAME");
                String  email = rs.getString("EMAIL");
                System.out.println( "ID = " + id2 );
                System.out.println( "USERNAME = " + username );
                System.out.println( "EMAIL = " + email );
                System.out.println();
            }

            rs.close();
            stmt.close();
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }





    }
}


class Person{
    private String username;
    private String email;

    public Person(String username, String email){
        this.username = username;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        int result = email.hashCode();
        return result;
    }
}